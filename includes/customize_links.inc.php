<?php
/**
 * @file
 * Custom Links Class
 *
 * Helper class to list, insert, update and delete
 *  customized links from customize_links schema
 *
 * Customize Links allows users to add custom paths for links allowing users
 * to specify query parameters for links to be used later on .
 */


class CustomizeLinks {
  protected $listLinks = array();
  protected $listLinksProcessed = array();

  /**
   * Return All links in customize_schema.
   *
   * These links can be processed or retrieved.
   *
   * @return Object
   *   For chaining to process or retrieve links
   */
  public function getAll() {
    $query = db_select('customize_links', 'cl')
      ->fields('cl', array('id', 'title', 'path', 'category'));
    $this->listLinks = $query->orderBy('created', 'DESC')
      ->execute()
      ->fetchAllAssoc('id', PDO::FETCH_ASSOC);
    return $this;
  }

  /**
   * Return a link where Link ID = supplied $id.
   *
   * @param int $id
   *   Link ID
   *
   * @return array
   *   Result set with retrieved link (if any)
   */
  public function get($id) {
    return db_select('customize_links', 'cl')
      ->fields('cl', array('id', 'title', 'path', 'category'))
      ->condition('id', $id)
      ->execute()
      ->fetchAssoc();
  }

  /**
   * Takes user supplied link information to insert in database.
   *
   * @param array $data
   *   Link data to insert; title, path, category
   *
   * @return int
   *   Insert ID
   */
  public function insert(Array $data) {
    return db_insert('customize_links')
        ->fields(array(
          'title' => $data['title'],
          'path' => $data['path'],
          'category' => $data['category'],
          'created'  => date("Y-m-d H:i:s", time()),
        ))->execute();
  }

  /**
   * Update Link data for provided Link ID.
   *
   * @param int $id
   *   Link ID to update
   * @param array $data
   *   Link data to update; title, path, category
   *
   * @return int
   *   Number of rows affected in update
   */
  public function update($id, Array $data) {
    return db_update('customize_links')
      ->fields(array(
        'title' => $data['title'],
        'path' => $data['path'],
        'category' => $data['category'],
        'updated'  => date("Y-m-d H:i:s", time()),
      ))
      ->condition('id', $id)
      ->execute();
  }

  /**
   * Loop through entire list of Links and insert those in database.
   *
   * @param array $links
   *   Links to be imported; title, path, category
   *
   * @return bool
   *   TRUE
   */
  public function import(Array $links) {
    foreach ($links as $link) {
      $this->insert($link);
    }
    return TRUE;
  }

  /**
   * Delete Links based on provided IDs.
   *
   * @param array $ids_to_remove
   *   All Link IDs to remove
   *
   * @return interface
   *   DatabaseStatementInterface
   */
  public function delete(Array $ids_to_remove) {
    return db_delete('customize_links')
        ->condition('id', $ids_to_remove, 'in')
        ->execute();
  }

  /**
   * Process retrieved links and arrange those by category.
   *
   * This list can be used in block to group all links by category
   *
   * @return object
   *   Class for chaining to process or retrieve links
   */
  public function processResult() {
    foreach ($this->listLinks as $id => $link) {
      $this->listLinksProcessed[$link['category']][$id] = $link;
    }
    return $this;
  }

  /**
   * Return list of links.
   *
   * @return array
   *   List of Links
   */
  public function getLinks() {
    return $this->listLinks;
  }

  /**
   * Return list of processed links.
   *
   * @return array
   *   List of Processed Links
   */
  public function getLinksProcessed() {
    return $this->listLinksProcessed;
  }
}
