-- SUMMARY --

Customize Links allows users to create custom links and assign category.
These links will show up in list format in Custom Links blocks.
That block can be used anywhere but mostly it can be used in dashboard to
create admin links. Although, this will use normal paths, it will allow users
to add custom queries in the links for modules like pre-populate or other needs

One example is link to Node Add page but with custom parameters.


-- REQUIREMENTS --

None.


-- INSTALLATION --

* Install as usual, see https://drupal.org/documentation/install/modules-themes/modules-7 for further information.
* "custom_links schema will be created on install

-- CONFIGURATION --

* Configure user permissions in Administration » People » Permissions:

  - "administer custom links"

    Role who can administer links

-- FAQ --

-- CONTACT --

Current maintainers:
* Jaswinder S. Rattanpal (jrattanpal) - https://drupal.org/user/1581558
